console.log('Génération d\'une clé pour JWT\n\n')
let tok = require('crypto').randomBytes(64).toString('hex') //module interne à nodejs
console.log('token : ' + tok)
console.log('\n\n Cette clé doit être indiquer dans le fichier .env JWT_KEY=<token>')
console.log('\n Si vous déployez sur IBM Cloud indiquez la variable JWT_KEY dans variable d\'environnement')
console.log(' dans onglet Exécution de votre application cloud foundry après deploiement')
console.log('\n\n')
