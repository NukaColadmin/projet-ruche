///////////////////////////////////////////////////////
//  ProjetRuche V0.1.2
//  Raphaël Janson
//  08/05/20
///////////////////////////////////////////////////////

//STORE REACT
import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { selectorDashboard, selectorConsulter, selectorGraph } from './actions'

//RAPH'S COMPONENTS INCLUDE
import Ruche from './components/Ruche.js'
import Registre from './components/Registre.js'
import Graph from './components/Graph.js'

//MATERIAL UI INCLUDE
import { makeStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ListAltIcon from '@material-ui/icons/ListAlt';
import TimelineIcon from '@material-ui/icons/Timeline';

//Style materialUI
const useStyles = makeStyles(theme => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  list: {
    width: 250,
  },
  version: {
    textAlign: 'center'
  }
}));

export default function App() {
  const classes = useStyles(); //constante de style
  const selector = useSelector(state => state.selector) //index de sélection du menu
  const dispatch = useDispatch() //Redux -> Enregistrement état globale

  //Etat local
  const [state, setState] = React.useState({
    left: false,
    selectedIndex: 1,
    setSelectedIndex: 1,
  });

  //Evenement ouverture du menu
  const toggleDrawer = (side, open) => event => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, [side]: open });
  };

  //Evenement click sur une Rubrique
  const handleListItemClick = (event, index) => {
    if(index === 0) {
      dispatch(selectorDashboard())
    }
    else if (index === 1) {
      dispatch(selectorConsulter())
    }
    else {
      dispatch(selectorGraph())
    }
  };

  //Titre du menu
  const setTitle = () => {
    if (selector === 0) {
      return "Dashboard"
    }
    else if (selector === 1) {
      return "Registre Elevage"
    }
    else {
      return "Graphique"
    }
  }

  //Rubrique à afficher
  const setRendering = () => {
    if (selector === 0) {
      return <Ruche />
    }
    else if (selector === 1) {
      return <Registre />
    }
    else {
      return <Graph />
    }
  }

  //Le menu lui meme (voir API materialUI)
  const sideList = side => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        <ListItem
          button
          selected={state.selectedIndex === 0}
          onClick={event => handleListItemClick(event, 0)}
        >
          <ListItemIcon>
            <DashboardIcon />
          </ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
        <ListItem
          button
          selected={state.selectedIndex === 1}
          onClick={event => handleListItemClick(event, 1)}
        >
          <ListItemIcon>
            <ListAltIcon />
          </ListItemIcon>
          <ListItemText primary="Registre Elevage" />
        </ListItem>
        <ListItem
          button
          selected={state.selectedIndex === 2}
          onClick={event => handleListItemClick(event, 2)}
        >
          <ListItemIcon>
            <TimelineIcon />
          </ListItemIcon>
          <ListItemText primary="Graphique" />
        </ListItem>
      </List>
    </div>
  );

//Affichage de notre menu et du bouton login
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <IconButton onClick={toggleDrawer('left', true)} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            {setTitle()}
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
      <Drawer open={state.left} onClose={toggleDrawer('left', false)}>
        {sideList('left')}
      </Drawer>
      {setRendering()}
      <Typography align="center" variant="body2">
        version client 0.1.2 |
        <Link href="/srclogo.html" rel="noreferrer" target="_blank">
          {' credits - licences '}
        </Link>
      </Typography>
    </div>
  );
}
