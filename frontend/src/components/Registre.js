//Bibliotèques React/Redux
import React from 'react';

//Composants Perso
import Excel from './Excel.js'
import Alertes from './Alertes.js'

//Material UI
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import SaveIcon from '@material-ui/icons/Save';

//Styles MaterialUI
const useStyles = makeStyles(theme => ({
  paper: {
    margin: "auto",
    width: "90%",
    maxWidth: "400px",
    height: "100%",
    marginBottom: "5%",
  },
  textfield: {
    width: "80%",
    marginLeft: "5%",
    marginTop: "5%",
  },
  checkbox:{
    marginLeft: "5%",
    marginTop: "5%",
  },
  typo:{
    position: "auto",
    textAlign: "left",
    marginLeft: "5%",
    marginTop: "5%"
  },
  btnSauvegarder:{
    marginLeft: "55%",
    marginTop: "5%",
    marginBottom: '2%',
  },
}));

export default function Registre() {
  const classes = useStyles(); //constante de Styles

//Notre etat
  const [state, setState] = React.useState({
    errorRuche: true,
    textfieldOrigine: "",
    textfieldManipulations: "",
    textfieldRecolte: "",
    textfieldRemarques: "",
    checkedSirop: false,
    checkedMiel: false,
    checkedTVarroa: false,
    checkedTLoques: false,
    checkedTNosemose: false,
    checkedMVarroa: false,
    checkedMLoques: false,
    checkedMNosemose: false,
});

//Evenement de changement d'états des checkbox
  const handleChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

//Evenement de changement d'états des textfield
  const handleChangeText = event => {
    setState({ ...state, [event.target.name]: event.target.value});
  }

//Evenement de changement d'état du champ textfieldOrigine
  const handleChangeTextOrigine = event => {
    //Champ vide ?
    if (event.target.value === '') {
      setState({...state,
        errorRuche: true,
        errorMessage: "Le champ origine ruche ne doit pas être vide",
        errorType: "warning"
      })
    } else {
      //setState({ ...state, [event.target.name]: event.target.value, errorRuche: false});
      setState({ ...state, [event.target.name]: event.target.value, errorRuche: false});
    }
  }

//Affichage du registre
//Voir l'API de materialUI
 return (
  <div>
    <Paper className={classes.paper} elevation={2}>
      <Typography className={classes.typo} variant="h6">
        Fiche Journalière
      </Typography>
      <TextField
        className={classes.textfield}
        required id="origine-colonie"
        onChange={handleChangeTextOrigine}
        name="textfieldOrigine"
        label="Origine Ruche"
        error={state.errorRuche}
      />
      <TextField
        className={classes.textfield}
        name="textfieldManipulations"
        id="manipulation-colonie"
        label="Manipulations"
        onChange={handleChangeText}
        multiline
        rows="4"
        variant="outlined"
      />
      <TextField
        label="Récolte en KG"
        name="textfieldRecolte"
        onChange={handleChangeText}
        id="recolte-colonie"
        className={classes.textfield}
        InputProps={{
          endAdornment: <InputAdornment position="end">Kg</InputAdornment>,
        }}
      />
      <FormLabel className={classes.checkbox} component="legend">Nourrissements :</FormLabel>
      <FormGroup className={classes.checkbox} row>
        <FormControlLabel
          control={
            <Checkbox
              className={classes.checkbox}
              checked={state.checkedSirop}
              onChange={handleChange}
              name="checkedSirop"
              color="primary"
            />
          }
          label="Sirop"
        />
        <FormControlLabel
          control={
            <Checkbox
              className={classes.checkbox}
              onChange={handleChange}
              checked={state.checkedMiel}
              name="checkedMiel"
              color="primary"
            />
          }
          label="Miel"
        />
      </FormGroup>
      <Divider/>
      <FormControl component="fieldset">
        <FormLabel className={classes.checkbox} component="legend">Traitements :</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedTVarroa}
              color="primary"
              name="checkedTVarroa" />}
            label="Varroase"
            className={classes.checkbox}
          />
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedTLoques}
              color="primary"
              name="checkedTLoques" />}
            label="Loques"
            className={classes.checkbox}
          />
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedTNosemose}
              color="primary"
              name="checkedTNosemose" />}
            label="Nosémose"
            className={classes.checkbox}
          />
        </FormGroup>
      </FormControl>
      <FormControl component="fieldset">
        <FormLabel color="secondary" className={classes.checkbox} component="legend">Maladies :</FormLabel>
        <FormGroup>
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedMVarroa}
              color="secondary"
              name="checkedMVarroa" />}
            label="Varroase"
            className={classes.checkbox}
          />
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedMLoques}
              color="secondary"
              name="checkedMLoques" />}
            label="Loques"
            className={classes.checkbox}
          />
          <FormControlLabel
            control={<Checkbox
              onChange={handleChange}
              checked={state.checkedMNosemose}
              color="secondary"
              name="checkedMNosemose" />}
            label="Nosémose"
            className={classes.checkbox}
          />
        </FormGroup>
      </FormControl>
      <TextField
        className={classes.textfield}
        id="remarques-colonie"
        name="textfieldRemarques"
        onChange={handleChangeText}
        label="Autres remarques"
        multiline
        rows="4"
        variant="outlined"
      />
    {state.errorRuche ? <Alertes errorType={state.errorType} message={state.errorMessage}/> : ""}
    {state.errorRuche ? <Button className={classes.btnSauvegarder} disabled variant="contained" color="primary" startIcon={<SaveIcon />}> Sauvegarder </Button> : <Excel data={state}/>}
    </Paper>
  </div>
);
}
