//Bibliotèques React/Redux
import React from 'react';
import { useSelector } from 'react-redux'

//Composants Perso
import Alertes from './Alertes.js'
import envoyerLogs from './Logs.js'

//Bootstrap material-ui
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';
import Skeleton from '@material-ui/lab/Skeleton';
import EditIcon from '@material-ui/icons/Edit';

//Bibliotèques Annexes
import Axios from 'axios'

const useStyles = makeStyles(theme => ({
  edit: {
    left: "90%"
  }
}));

export default function Options() {
  const classes = useStyles(); //constante de Styles
  const indexRuche = useSelector(state => state.indexRuche) //Index de la ruche active
  const mesures = useSelector(state => state.mesures) //Derniere mesures de la ruche

  const [open, setOpen] = React.useState(false); //ouveture/fermeture du dialogue
//Etat initial
  const [state, setState] = React.useState({
    textFieldValueNom: 'Erreur',
    textFieldValueLocalisation: 'Erreur',
    btnSauvegarder: false,
  });

//Dialogue Ouvert
  const handleClickOpen = () => {
    setState({
      textFieldValueNom: mesures[indexRuche].nom ? mesures[indexRuche].nom : "Pas définie",
      textFieldValueLocalisation: mesures[indexRuche].localisation ? mesures[indexRuche].localisation : "Pas définie"
    })
    setOpen(true); //set Ouvert
  };

//Dialogue Fermée
  const handleClose = () => {
    //Reinitialisation des paramamètres
    setState({
      textFieldValueNom: 'Pas définie',
      textFieldValueLocalisation: 'Pas définie',
      errorRuche: false,
      btnSauvegarder: false,
    })
    setOpen(false); //set Fermée
  };

//Action Boutton Sauvegarder
  const handleCloseSauvegarder = () => {

    //Enregistrement des infos dans la BDD si valeurs modifié sinon dialogue fermé
    Axios.put('/api/edit/' + mesures[indexRuche].id, {
    nom: state.textFieldValueNom,
    localisation: state.textFieldValueLocalisation,
    })
    .then(function (reponse) {
      handleClose()
    })
    .catch(function (error) {
      envoyerLogs("ERROR", error , "EO106" )
      setState({...state,
        errorRuche: true,
        errorType: "error",
        errorMessage: "Pas de connexion avec l'API ou utilisateur non connecté EO106",
      })
    });
  }

//Evenement changement d'états des textfields
  const handleTextFieldChange = (event) => {
    if (event.target.value === '' || event.target.value === false){
      setState({...state,
        [event.target.name]: 'Pas encore défini',
      })
    } else {
      setState({...state,
        [event.target.name]: event.target.value,
      })
    }
  }

  return (
    <div>
      <Fab color="secondary" onClick={handleClickOpen} aria-label="edit" className={classes.edit} size="small">
        <EditIcon/>
      </Fab>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"> Editer la ruche {mesures ? (mesures[indexRuche].id ? mesures[indexRuche].id : <Skeleton variant="text" />) : <Skeleton variant="text" />} </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Modification des valeurs de la ruche (nom, localisation).
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="textFieldValueNom"
            id="nom"
            onChange={handleTextFieldChange}
            label="Nom de la ruche"
            defaultValue={state.textFieldValueNom}
            type="text"
            fullWidth
          />
          <TextField
            autoFocus
            margin="dense"
            name="textFieldValueLocalisation"
            id="localisation"
            onChange={handleTextFieldChange}
            label="Localisation"
            defaultValue={state.textFieldValueLocalisation}
            type="text"
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Annuler
          </Button>
          <Button disabled={state.btnSauvegarder} onClick={handleCloseSauvegarder} color="primary">
            Sauvegarder
          </Button>
        </DialogActions>
      </Dialog>
      {state.errorRuche ? <Alertes errorType={state.errorType} message={state.errorMessage}/> : null}
    </div>
  );
}
