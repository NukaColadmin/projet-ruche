import {format} from 'date-fns';

export default function envoyerLogs(errorType, errorMessage, rubrique) {
  //Création d'un objet Date
  let time = new Date()
  //envoi des logs
  console.log(format(time, "yyyy-MM-dd'T'HH:mm:ss.'000Z'")
    + ' | ' + errorType + ' | ' + errorMessage + ' | ' + rubrique + ' | '
    + navigator.userAgent
  );
}
