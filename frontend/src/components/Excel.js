//React import
import React from "react";

//Material UI
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';

//Autres Bibliotèques
//Voir l'API de react-export-excel
import ReactExport from "react-export-excel";

const ExcelFile = ReactExport.ExcelFile; //Fichier
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet; //Feuille de classeur
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn; //Colonnes

//Styles MaterialUI
const useStyles = makeStyles(theme => ({
  button: {
    marginLeft: "55%",
    marginTop: "5%",
    marginBottom: '2%',
  },
}));

export default function Excel(props) {
  const classes = useStyles();
  let time = new Date() //Creation d'un objet Date

  //Le data par lignes (props = paramètres)
  let dataSet1 = [
      {
          origineRuche: props.data.textfieldOrigine,
          manipulationsRuche: props.data.textfieldManipulations,
          recolteRuche: props.data.textfieldRecolte,
          remarquesRuche: props.data.textfieldRemarques,
          is_Sirop: props.data.checkedSirop,
          is_Miel: props.data.checkedMiel,
          is_TVarroa: props.data.checkedTVarroa,
          is_TLoques: props.data.checkedTLoques,
          is_TNosemose: props.data.checkedTNosemose,
          is_MVarroa: props.data.checkedMVarroa,
          is_MLoques: props.data.checkedMLoques,
          is_MNosemose: props.data.checkedMNosemose,
      },
  ];

//Voir API
  return (
    <ExcelFile
      element={<Button className={classes.button} variant="contained" color="primary" startIcon={<SaveIcon />}> Sauvegarder </Button>}
      filename={props.data.textfieldOrigine + time.getDate() + (time.getMonth()+1) + time.getFullYear()}>
      <ExcelSheet data={dataSet1} name={props.data.textfieldOrigine + '_' + time.getFullYear() + '-' + (time.getMonth()+1) + '-' + time.getDate()}>
        <ExcelColumn label="Origine Colonie" value="origineRuche" />
        <ExcelColumn label="Manipulations" value="manipulationsRuche" />
        <ExcelColumn label="Récolte (KG)"
          value={(col) => col.recolteRuche ? col.recolteRuche : "0"} />
        <ExcelColumn label="Sirop"
          value={(col) => col.is_Sirop ? "oui" : "non"} />
        <ExcelColumn label="Miel"
          value={(col) => col.is_Miel ? "oui" : "non"} />
        <ExcelColumn label="Traitement Varroa"
          value={(col) => col.is_TVarroa ? "oui" : "non"} />
        <ExcelColumn label="Traitement Loques"
          value={(col) => col.is_TLoques ? "oui" : "non"} />
        <ExcelColumn label="Traitement Nosémose"
          value={(col) => col.is_TNosemose ? "oui" : "non"} />
        <ExcelColumn label="Varroa?"
          value={(col) => col.is_MVarroa ? "oui" : "non"} />
        <ExcelColumn label="Loques?"
          value={(col) => col.is_MLoques ? "oui" : "non"} />
        <ExcelColumn label="Nosémose?"
          value={(col) => col.is_MNosemose ? "oui" : "non"} />
        <ExcelColumn label="Autres remarques" value="remarquesRuche" />
      </ExcelSheet>
    </ExcelFile>
  )
}
