//Bibliotèques React/Redux
import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { mesuresRuche, indexRucheIncrement, indexRucheDecrement } from '../actions'

//Composants Perso
import Alertes from './Alertes.js'
import Options from './Options.js'
import envoyerLogs from './Logs.js'

//Bootstrap material-ui
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Skeleton from '@material-ui/lab/Skeleton';
import Zoom from '@material-ui/core/Zoom';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

//Bibliotèques Annexes
import Axios from 'axios'

//Style Material-ui
const useStyles = makeStyles(theme => ({
  boutons: {
    textAlign: 'center',
  },
  img: {
    display: 'block',
    marginTop: '2%',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  list: {
    margin: 'auto',
    width: "90%",
    maxWidth: "400px",
    marginBottom: '5%',
  },
  typo: {
    margin: 'auto',
    textAlign: 'center',
  },
}));

const publicdir = process.env.PUBLIC_URL //Alias Repertoire Publique

//Composant ruche
export default function Ruche() {
  const classes = useStyles(); //constante de Styles
  const indexRuche = useSelector(state => state.indexRuche) //Index de la ruche active
  const mesures = useSelector(state => state.mesures) //Derniere mesures de la ruche
  const dispatch = useDispatch() //Alias pour le stockage dans l'etat global (Redux)

//Etat local
  const [state, setState] = React.useState({
    errorRuche: false,
  });



//Fonction joué au lancement de l'application
  useEffect(() => {
    //Actualisation des mesures des ruches
      const actualiserMesures = () => {
          Axios.get('/api/mesures') //requete en GET des mesures
            .then(function (reponse) {
              if (reponse.data.ruches.length <= 0
                || reponse.data.ruches === undefined
                || reponse.data.ruches === false
                || reponse.data.ruches === null
              ) {
                envoyerLogs("ERROR", "Mesures Vide", "ERU104" )
                setState({
                  errorRuche: true,
                  errorType: "error",
                  errorMessage: "API connectée mais pas de mesures… ERU104",
                })
              }
              else {
                //envoi des mesures dans le store
                dispatch(mesuresRuche(reponse.data.ruches))
                envoyerLogs("INFO", "GETRES" , "IRU101" )
                setState({errorRuche: false})
              }
            })
            .catch(function (error) {
              //Sinon envoi de l'erreur dans les logs
              envoyerLogs("ERROR", error , "GETRES" )
              setState({
                errorRuche: true,
                errorType: "error",
                errorMessage: "L’API ne réponds pas ERU105",
              })
            })
      }

    actualiserMesures()
    const surveiller = setInterval(() => {
      actualiserMesures()
    }, 60000) //Intervale de temps entre chaques ticks de la fonction en ms
    return () => clearInterval(surveiller)
  }, [dispatch]);

//Action bouton précédent
  const handlePrev = () => {
    if (indexRuche > 0){
      dispatch(indexRucheDecrement())
    }
  }

//Action bouton suivant
  const handleNext = () => {
    if (indexRuche < Object.keys(mesures).length - 1){
      dispatch(indexRucheIncrement())
    }
  }

//Affichage JSX de notre application
//operation tern => {condition ? true : false}
  return (
    <div>
      <img className={classes.img} src={publicdir + "/logos/hive.png"} alt="ruches" height="150" width="150"/>
      <div className={classes.boutons}>
        <IconButton onClick={handlePrev} disabled={state.errorRuche} aria-label="bouton precedent">
          <ArrowBackIosIcon/>
        </IconButton>
        <IconButton onClick={handleNext} disabled={state.errorRuche} aria-label="bouton suivant">
          <ArrowForwardIosIcon/>
        </IconButton>
      </div>
      <List className={classes.list}>
          <Paper elevation={2}>
            <Typography className={classes.typo} variant="h5">
              {mesures ? (mesures[indexRuche].nom ? mesures[indexRuche].nom : <Skeleton variant="text" />) : <Skeleton variant="text" />}
            </Typography>
            <Divider />
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="ID" src={publicdir + "/logos/id.png"} />
              </ListItemAvatar>
              <ListItemText primary="ID" secondary={mesures ? (mesures[indexRuche].id ? mesures[indexRuche].id : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Nom" src={publicdir + "/logos/dog-tag.png"} />
              </ListItemAvatar>
              <ListItemText primary="Nom Ruche" secondary={mesures ? (mesures[indexRuche].nom ? mesures[indexRuche].nom : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Localisation" src={publicdir + "/logos/place.png"} />
              </ListItemAvatar>
                <ListItemText primary="Localisation" secondary={mesures ? (mesures[indexRuche].localisation ? mesures[indexRuche].localisation : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <Divider />
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Poids" src={publicdir + "/logos/weight-scale.png"} />
              </ListItemAvatar>
                <ListItemText primary="Poids" secondary={mesures ? (mesures[indexRuche].poids ? mesures[indexRuche].poids + " KG" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Luminositée" src={publicdir + "/logos/sun.png"} />
              </ListItemAvatar>
                <ListItemText primary="Luminositée" secondary={mesures ? (mesures[indexRuche].lum ? mesures[indexRuche].lum + " Lux" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Température" src={publicdir + "/logos/hot.png"} />
              </ListItemAvatar>
              <ListItemText primary="Température Intérieur/Extérieur" secondary={mesures ? (mesures[indexRuche].temperatureI || mesures[indexRuche].temperatureE ? mesures[indexRuche].temperatureI + " °C / " + mesures[indexRuche].temperatureE + " °C" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Vent" src={publicdir + "/logos/wind.png"} />
              </ListItemAvatar>
              <ListItemText primary="Vent" secondary={mesures ? (mesures[indexRuche].vent ? mesures[indexRuche].vent + " KM/h" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Humiditée" src={publicdir + "/logos/water.png"} />
              </ListItemAvatar>
              <ListItemText primary="Humiditée Intérieur/Extérieur" secondary={mesures ? (mesures[indexRuche].humiditeeI || mesures[indexRuche].humiditeeE ? mesures[indexRuche].humiditeeI + " % / " + mesures[indexRuche].humiditeeE + " %" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <ListItem>
              <ListItemAvatar>
                <Avatar variant="square" alt="Abeilles" src={publicdir + "/logos/bee.png"} />
              </ListItemAvatar>
              <ListItemText primary="Abeilles Entrée/Sortie" secondary={mesures ? (mesures[indexRuche].abeille_in || mesures[indexRuche].abeille_out ? mesures[indexRuche].abeille_in + " / " + mesures[indexRuche].abeille_out + " abeilles" : <Skeleton variant="text" />) : <Skeleton variant="text" />} />
            </ListItem>
            <Zoom in={!state.errorRuche}>
              <div className={classes.btnEdit}>
                <Options/>
              </div>
            </Zoom>
          </Paper>
      </List>
      {state.errorRuche ? <Alertes errorType={state.errorType} message={state.errorMessage}/> : ""}
    </div>
  )
}
