//Bibliotèques React/Redux
import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
  mesuresGraph, indexRucheIncrement, indexRucheDecrement, setDate, setFiltre
} from '../actions'

//Graphiques
import {
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,
  Legend,
} from 'recharts';

//Bibliotèque MaterialUI
import {format} from 'date-fns';
import { DateTimePicker } from "@material-ui/pickers";
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepContent from '@material-ui/core/StepContent';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Slider from '@material-ui/core/Slider';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import IconButton from '@material-ui/core/IconButton';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

//Bibliotèques Annexes
import Axios from 'axios'

//Composants Perso
import Alertes from './Alertes.js'
import envoyerLogs from './Logs.js'

const useStyles = makeStyles(theme => ({
  typo: {
    margin: 'auto',
    textAlign: 'center',
  },
  btnSuivPrec: {
    textAlign: 'center',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(1),
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  resetContainer: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
  },
}));

//A mettre avant la definition de la constante 'steps'
//Les differentes etapes
const getSteps = () => {
  return ['Choisir un filtre', 'Choisir une date de référence'];
}

export default function Graph() {
  const classes = useStyles(); //constante de Styles
  const graphData = useSelector(state => state.graphData) //Derniere mesures de la ruche
  const dateGraph = useSelector(state => state.date)
  const filtre = useSelector(state => state.filtre) //le filtre (plage de frequence)
  const mesures = useSelector(state => state.mesures) //Derniere mesures de la ruche
  const indexRuche = useSelector(state => state.indexRuche)
  const dispatch = useDispatch() //Alias pour le stockage dans l'etat global (Redux)
  const steps = getSteps() //les index des etapes

  //Etat local
    const [activeStep, setActiveStep] = React.useState(0);
    const [state, setState] = React.useState({
      errorRuche: false,
    });

//les differentes mesures possible
    const marks = [
      {
        value: 1,
        label: '24H',
      },
      {
        value: 3,
        label: '3J.',
      },
      {
        value: 7,
        label: '1Sem.',
      },
      {
        value: 30.4167,
        label: '1Mois',
      },
    ];

    //Contenu de chaques steps
    const getStepContent = (step) => {
      switch (step) {
        case 0:
          return (
              <Slider
                defaultValue={7}
                valueLabelFormat={handleLabelIndexSlider}
                getAriaValueText={handleValueChange}
                aria-labelledby="discrete-slider-restrict"
                step={null}
                min={1}
                max={30.4167}
                valueLabelDisplay="auto"
                marks={marks}
              />
          );
          case 1:
            return (
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <DateTimePicker
                ampm={false}
                disableFuture
                value={dateGraph}
                onChange={handleDateChange}
                label="Date Référence"
                format="yyyy-MM-dd'T'HH:mm:ss.'000Z'"
              />
              </MuiPickersUtilsProvider>
            )
        default:
          envoyerLogs("ERROR", "Etape introuvable", "EG302")
      }
    }
    //Actualisation du graphique et des mesures
    const actualiserGraphique = () => {
      //L'id doit etre de 16 caractères
      if (
        mesures[indexRuche] === undefined
        || mesures[indexRuche].id === undefined
        || mesures[indexRuche].id === false
        || mesures[indexRuche].id === null
        || mesures[indexRuche].id.length !== 16
      ) {
        envoyerLogs("ERROR", "Erreur envoi id non conforme EG203", "EG203" )
        setState({
          errorRuche: true,
          errorType: "error",
          errorMessage: "Erreur envoi id non conforme EG203",
        })
      }
      else if (!(dateGraph instanceof Date)) {
        envoyerLogs("ERROR", "Erreur envoi date non conforme EG202", "EG202" )
        setState({
          errorRuche: true,
          errorType: "error",
          errorMessage: "Erreur envoi date non conforme EG202",
        })
      }
      else {
        //requete en GET des mesures
        setState({errorRuche: false})
        let result = new Date(dateGraph)
        result.setDate(result.getDate() + filtre)
        //On fait une copie de la date de ref. et on ajoute le curseur
        Axios.get('/api/graph/' + mesures[indexRuche].id + '/' + format(dateGraph, "yyyy-MM-dd'T'HH:mm:ss.'000Z'") + '/' + format((result), "yyyy-MM-dd'T'HH:mm:ss.'000Z'"))
          .then(function (reponse) {
            if (reponse.data === false || reponse.data === undefined)
            {
              envoyerLogs("ERREUR", "Réponse vide EG107", "EG107" )
              setState({
                errorRuche: true,
                errorType: "error",
                errorMessage: "Réponse vide EG107",
              })
            }
            else {
              //Liste des valeurs de la reponse avec un switch
              let message, num
              for (let [key, value] of Object.entries(reponse.data)) {
                switch (key) {
                  case 'id':
                    num = 'EG102'
                    break;
                  case 'nom':
                    num = 'EG103'
                    break;
                  case 'debut' || 'fin':
                    num = 'EG104'
                    break;
                  case 'data':
                    num = 'EG105'
                    message = 'Erreur data (pas de mesures) EG105'
                    break;
                  case 'external':
                    num = 'EG106'
                    message = 'Erreur external (pas de mesures) EG106'
                    break;
                  default:
                    message = 'No Erreur'
                }
                if (value === null || value === '' || value === undefined || value === false || value.length === 0) {
                  envoyerLogs("ERREUR", message, num )
                  setState({
                    errorRuche: true,
                    errorType: "error",
                    errorMessage: message,
                  })
                  //Arret de la fonction si erreur
                  return 1;
                }
              }
              dispatch(mesuresGraph(reponse.data))
              envoyerLogs("INFO", "GETRES" , "IG101" )
              setState({
                errorRuche: true,
                errorType: "success",
                errorMessage: "Mesures Trouvées !"
              })
            }
          })
          .catch(function (error) {
            setState({
              errorRuche: true,
              errorType: "error",
              errorMessage: "Pas de connexion avec l'API EG108" + String(error),
            })
            envoyerLogs("ERROR", error , "EG108" )
          })
      }
    }

  //Action bouton pour passer à l'etape suivante
    const handleNextStep = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1)
      //Action bouton 'OK'
      if (activeStep === 1) {
        actualiserGraphique()
      }
    }
  //Action bouton pour revenir à l'étape precedente
    const handleBackStep = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1)
    }
  //Action bouton Reset
    const handleResetStep = () => {
      setActiveStep(0);
    };

//Action bouton précédent
    const handlePrev = () => {
      if (indexRuche > 0){
        dispatch(indexRucheDecrement())
        actualiserGraphique()
      }
    }
  //Action bouton suivant
    const handleNext = () => {
      if (indexRuche < Object.keys(mesures).length - 1){
        dispatch(indexRucheIncrement())
        actualiserGraphique()
      }
    }
  //Passe à l'index suivant pour afficher le label dans "marks"
    const handleLabelIndexSlider = (value) => {
      return marks.findIndex(mark => mark.value === value) + 1
    }
  //Action pour le changement de valeur du slider
    const handleValueChange = (value) => {
      if (value !== filtre) {
        dispatch(setFiltre(value))
      }
    }
  //changement de valeur du calendrier (date réference)
    const handleDateChange = (value) => {
      dispatch(setDate(value))
    }

  return (
    <div>
      <Stepper activeStep={activeStep} orientation="vertical">
        {steps.map((label, index) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
            <StepContent>
              {getStepContent(index)}
              <div className={classes.actionsContainer}>
                <div>
                  <Button
                    disabled={activeStep === 0}
                    onClick={handleBackStep}
                    className={classes.button}
                  >
                    Précédent
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleNextStep}
                    className={classes.button}
                  >
                    {activeStep === steps.length - 1 ? 'OK' : 'Suivant'}
                  </Button>
                </div>
              </div>
            </StepContent>
          </Step>
        ))}
      </Stepper>
      {activeStep === steps.length && (
        <Paper square elevation={0} className={classes.resetContainer}>

          <Button
            variant="outlined"
            onClick={handleResetStep}
            className={classes.button}
          >
            Reinitialiser les filtres
          </Button>
        </Paper>
      )}
      <Typography className={classes.typo} variant="h5">
        {mesures ? (mesures[indexRuche].nom ? mesures[indexRuche].nom : <Skeleton variant="text" />) : <Skeleton variant="text" />}
      </Typography>
      <div className={classes.btnSuivPrec}>
        <IconButton onClick={handlePrev} disabled={state.errorRuche} aria-label="bouton precedent">
          <ArrowBackIosIcon/>
        </IconButton>
        <IconButton onClick={handleNext} disabled={state.errorRuche} aria-label="bouton suivant">
          <ArrowForwardIosIcon/>
        </IconButton>
      </div>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.data} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Poids" type='monotone' dataKey='poids' stroke='#9F9F9F' strokeWidth={2} fill='#9F9F9F' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.external} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Luminositée" type='monotone' dataKey='lum' stroke='#ECEC0A' strokeWidth={2} fill='#ECEC0A' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.data} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Température Intérieur" type='monotone' dataKey='temperature' stroke='#DC0D0D' strokeWidth={2} fill='#DC0D0D' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.external} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Température Extérieur" type='monotone' dataKey='temperature' stroke='#0D75DC' strokeWidth={2} fill='#0D75DC' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.external} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Vent" type='monotone' dataKey='vent' stroke='#C3DDF6' strokeWidth={2} fill='#C3DDF6' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.data} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Humiditée Intérieur" type='monotone' dataKey='humidite' stroke='#DC0D0D' strokeWidth={2} fill='#DC0D0D' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.external} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Humiditée Extérieur" type='monotone' dataKey='humidite' stroke='#0D75DC' strokeWidth={2} fill='#0D75DC' />
      </AreaChart>
      </ResponsiveContainer>
      <ResponsiveContainer width="95%" height={200}>
      <AreaChart data={graphData.data} syncId="anyId"
            margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="date"/>
        <YAxis/>
        <Tooltip/>
        <Legend verticalAlign="top" height={36}/>
        <Area name="Abeilles entrée" type='monotone' dataKey='abeille_in' stroke='#908420' strokeWidth={2} fill='#908420' />
        <Area name="Abeilles sortie" type='monotone' dataKey='abeille_out' stroke='#3C370A' strokeWidth={2} fill='#3C370A' />
      </AreaChart>
      </ResponsiveContainer>
      {state.errorRuche ? <Alertes errorType={state.errorType} message={state.errorMessage}/> : ""}
    </div>
  );
}
