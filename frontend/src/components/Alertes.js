//Bibliotèques React/Redux
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

import envoyerLogs from './Logs.js'

//Nécessaire à l'affichage voir API materialUI
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

//Styles materialUI
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));

export default function Alertes(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true); //Notification ouverte ou fermée
  let error = false;

  //Evenement pour la fermeture de la Notification
  const handleClose = (event, reason) => {
    //Tester si clickaway (l'utilisateur à clicker autre part)
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false); //fermée
  };

  //message vide ?
  const is_Empty = () => {
    //Liste des valeurs des props
    for (let [key, value] of Object.entries(props)) {
      if (value === null || value === '' || value === undefined || value === false) {
        error = true
      }
    }
    if (error === true || props === undefined) {
      envoyerLogs("ERROR", "Pas de valeurs", "EAL104")
      return !open
    }

    else {
      return open
    }
  }

//Affichage de la Notification (voir API materialUI)
  return (
    <div className={classes.root}>
      <Snackbar open={is_Empty()} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={props.errorType}>
          {props.message}
        </Alert>
      </Snackbar>
    </div>
  );
}
