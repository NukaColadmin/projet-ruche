const menuButtonReducer = (state = 0, action) => {
  switch (action.type) {
    case 'DASHBOARD':
      return 0
    case 'CONSULTER':
      return 1
    case 'GRAPH':
      return 2
    default:
      return state
  }
}

export default menuButtonReducer
