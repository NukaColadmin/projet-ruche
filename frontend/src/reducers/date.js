const setDateReducer = (state = new Date(), action) => {
  switch (action.type) {
    case 'DATEREF':
      return action.data
    default:
      return state
  }
}

export default setDateReducer
