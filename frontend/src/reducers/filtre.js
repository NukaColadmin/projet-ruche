const setFiltreReducer = (state = 7, action) => {
  switch (action.type) {
    case 'SETFILTRE':
      return action.data
    default:
      return state
  }
}

export default setFiltreReducer
