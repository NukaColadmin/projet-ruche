//Les actions
import menuButtonReducer from './menuButton'
import mesuresRucheReducer from './mesuresRuche'
import rucheIndexReducer from './rucheIndex'
import mesuresGraphReducer from './mesuresGraph'
import dateGraphReducer from './date'
import filtreGraphReducer from './filtre'
import {combineReducers} from 'redux'

//Le state globale
const allReducers = combineReducers({
  selector: menuButtonReducer,
  mesures: mesuresRucheReducer,
  graphData: mesuresGraphReducer,
  indexRuche: rucheIndexReducer,
  date: dateGraphReducer,
  filtre: filtreGraphReducer,
})

export default allReducers
