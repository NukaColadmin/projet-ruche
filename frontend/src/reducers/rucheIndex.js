const rucheIndexReducer = (state = 0, action) => {
  switch (action.type) {
    case 'INDEXINCREMENT':
      return state + 1
    case 'INDEXDECREMENT':
      return state - 1
    default:
      return state
  }
}

export default rucheIndexReducer
