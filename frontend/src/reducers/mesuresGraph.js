const mesuresGraphReducer = (state = false, action) => {
  switch (action.type) {
    case 'ACTUALISERGRAPHIQUE':
      return action.data
    default:
      return state
  }
}

export default mesuresGraphReducer
