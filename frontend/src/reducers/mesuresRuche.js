const mesuresRucheReducer = (state = false, action) => {
  switch (action.type) {
    case 'ACTUALISER':
      return action.data
    default:
      return state
  }
}

export default mesuresRucheReducer
