//Les différentes actions
export const selectorDashboard = () => {
  return {
    type: 'DASHBOARD'
  }
}

export const selectorConsulter = () => {
  return {
    type: 'CONSULTER'
  }
}

export const selectorGraph = () => {
  return {
    type: 'GRAPH'
  }
}

export const mesuresRuche = res => {
  return {
    type: 'ACTUALISER',
    data: res
  }
}

export const mesuresGraph = res => {
  return {
    type: 'ACTUALISERGRAPHIQUE',
    data: res
  }
}

export const setDate = res => {
  return {
    type: 'DATEREF',
    data: res
  }
}

export const setFiltre = res => {
  return {
    type: 'SETFILTRE',
    data: res
  }
}

export const indexRucheIncrement = () => {
  return {
    type: 'INDEXINCREMENT',
  }
}

export const indexRucheDecrement = () => {
  return {
    type: 'INDEXDECREMENT',
  }
}
