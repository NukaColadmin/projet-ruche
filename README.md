# Projet Ruche Connectée
Pour le BTS SNIR session 2020
Hudon Guillaume && Janson Raphaël

## Sommaire
2. [Installation](#installation)
3. [Configuration](#configuration)
4. [Utilisation en Local](#utilisation-en-local)
5. [Déploiement sur IBM Cloud](#déploiement-sur-ibm-cloud)

**Pages Annexes :**
1. [Configuration IBM Cloud](./Documentation/ConfigurationCloudant.md)
2. [Configuration Cloudant](./Documentation/ConfigurationIBM.md)

**Documentation des Framework/Modules et Tutoriel Sources :**

1. [Tutoriel Déploiement](https://www.ibm.com/cloud/blog/react-web-express-api-development-production)
2. [Documentation express](https://expressjs.com/fr/4x/api.html)
3. [Documentation Cloudant SDK](https://github.com/cloudant/nodejs-cloudant)
4. [Dotenv Page](https://www.npmjs.com/package/dotenv)
5. [Site de ReactJS](https://reactjs.org/docs/getting-started.html)
6. [Site de MaterialUI](https://material-ui.com/getting-started/installation/)
7. [Site de date-fns](https://date-fns.org/)
8. [Site de MaterialUI-DateTimePicker](https://material-ui-pickers.dev/getting-started/installation)
9. [Site de ReduxJS](https://redux.js.org/introduction/getting-started)
10. [Site React Redux](https://react-redux.js.org/introduction/quick-start)
11. [Site de Recharts](https://recharts.org/en-US/guide/installation)
12. [GitHub d'Axios](https://github.com/axios/axios)
13. [Dépôt de react-export-excel](https://www.npmjs.com/package/react-export-excel)
14. [Documentation Déploiement Cloud Foundry](https://docs.cloudfoundry.org/devguide/deploy-apps/manifest-attributes.html#env-block)
15. [Exemple d'utilisation du module worker_threads](https://www.jesuisundev.com/comprendre-les-worker-threads-de-nodejs/)

---
### Installation
```bash
mkdir projetruche
cd projetruche
git clone https://gitlab.com/NukaColadmin/projet-ruche.git
```

Ce logiciel fonctionne avec [Node.js](https://nodejs.org/en/) via l'outil **npm** ou **yarn** en ligne de commande.

se rendre dans le dossier **frontend** puis exécutez la commande suivante :
~~~~bash
npm install
OU
yarn install
~~~~

toujours dans le dossier ```./frontend``` après l’installation des dépendances exécutez la commande suivante :
~~~~bash
npm run build
OU
yarn build
~~~~

Le build de notre application permet d'optimiser la mise en production

Retourner dans le dossier parent (ou la racine du projet) :
~~~~bash
cd ..
~~~~

puis exécuter la commande :

~~~~bash
npm install
OU
yarn install
~~~~

une fois l'installation terminée l'application est prête à être déployée ou exécutée localement par la commande.
~~~~bash
npm start
OU
yarn start
~~~~
>A exécuter sous le dossier racine et le dossier frontend
---
### Configuration

Si vous avez comme indiqué dans la documentation configuration du service Cloudant modifier les nom des base de données stocker dans Cloudant voici comment modifier les valeurs pour l'API :

Dans le dossier databaseList.json vous avez la liste des bases de données sur Cloudant c'est ici que vous allez modifier les nom des bases, la valeur a modifier est celle situer entre guillemet après les ":" le nom de la base doit être entre guillemet comme suit : _(la valeur Cloudant est à titre indicatif elle n'est pas utilisée par l'API)_
~~~~JSON
{
  "service": "Cloudant",
  "mesures": "test",
  "users": "users",
  "alerts":"alerts",
  "logs":"logs",
  "ruches":"ruches"
}
~~~~

#### Utilisation en local

Si vous utilisez l'application sur une machine local ou sur votre propre serveur créez un fichier ```.env``` dans la racine du projet et indiquée les valeurs suivante (retour à la ligne pour chaque valeurs):
````
cloudant_username=<username>
cloudant_password=<password>

````
Les valeurs sont celle obtenu lors de la configuration de Cloudant sur IBM Cloud dans les "Données d'identification pour le service"

Ajoutez également la valeur JWT_KEY dans le fichier ```.env``` pour l'utilisation de JWT
Pour générer une clé vous pouvez utiliser le script generateToken.js via la commande ```node generateToken.js``` le script affichera une clé générer à ajouter dans le fichier ```.env```
````
cloudant_username=<username>
cloudant_password=<password>
JWT_KEY=<clé génerer>
````

---
### Déploiement sur IBM Cloud

pour déployer sur IBM Cloud vous devez au préalable installer [IBM CLI](https://cloud.ibm.com/docs/cli?topic=cloud-cli-getting-started) :
~~~~bash
curl -sL https://ibm.biz/idt-installer | bash
~~~~
sous Windows 10 Pro (en mode admin) :
~~~~shell
[Net.ServicePointManager]::SecurityProtocol = "Tls12, Tls11, Tls, Ssl3"; iex(New-Object Net.WebClient).DownloadString('https://ibm.biz/idt-win-installer')
~~~~

Avant le déploiement de l'application sur le Cloud d'IBM vous devez définir des éléments dans le fichier manifest.yml (**RETIRER LES VALEUR EN CAS DE PUSH DANS UN REPO PUBLIC GITLAB/GITHUB**)

Les valeurs à indiquer sont les même quand le fichier ```.env``` si vous avez fait la configuration local, sinon il s'agit des credentials de votre service Cloudant et d'une clé JWT qui peut être générer par le script ```generateToken.js```.
~~~~yaml
env:
  cloudant_username: <username>
  cloudant_password: <password>
  JWT_KEY: <jwtkey>
~~~~

Dans le dossier racine du projet après identification sur ibmcloud, [voir Documentation IBM](https://cloud.ibm.com/docs/cli?topic=cloud-cli-getting-started), exécutez la commande suivante :
~~~bash
bluemix app push <APP_NAME>
~~~~

---


_version du 08/05/2020 |_
_HUDON Guillaume && Janson Raphaël_
