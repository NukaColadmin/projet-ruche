/////////////////////////////////
// API Rest 1.3.1
// version : 09/05/2020
//
// Guillaume HUDON
//
/////////////////////////////////

// déclaration des framework et library
const express = require('express'); // Express Serveur API
const fs = require('fs'); //Manipulation de fichiers
const bodyParser = require('body-parser');
require('dotenv').config(); // Environnement de l'API (définit dans .env ou sur l'interface cloud de l'application)
let Cloudant = require('@cloudant/cloudant'); // API Cloudant service IBM Cloud
let app = express(); // création de l'objet représentant notre application express
let port = process.env.PORT || 4000; // Définition du port par default 4000
const dev_logs = parseInt(process.env.DEV_LOG, 10) || 0; //Variable de debug pour voir des console log suplémentaire
const timezone_offset = process.env.timezone_offset || 0;// décalage horaire pour le cloud si besoin.
// Modules de JWT pour l'Authentification
var jwt = require('jsonwebtoken');
var expressJwt = require('express-jwt');

// Objet JSON pour appeler les noms des basses de données
let DBName;
try {
  let data = fs.readFileSync('./databaseList.json', 'utf8'); // Lecture du fichier
  DBName = JSON.parse(data);
} catch (err) {
  console.error(err);
}

// identification au service cloudant
var username = process.env.cloudant_username || "nodejs"; //définition credential user pour cloudant
var password = process.env.cloudant_password; // définition crédential password pour Cloudant
var cloudant = Cloudant({ account:username, password:password }); //Création de l'objet Cloudant
// La RFonction qui suit va vérifier que les bases de données indiquer
// dans le fichier databaseList.json existent si elles n'éxistent pas
//elles seront générer au démarage de l'API
initBases();

app.use(bodyParser.json());
app.use(express.static(`${__dirname}/frontend/build`));// initialisation de l'application WEB

/*********** Obtenir les dèrnières mesures des ruches*************/
app.get('/api/mesures', async (req, res) => {
  console.log('GET ' + getDateTime() + '] api/mesures \n');
  let retour_json = {"ruches":[]} // Initialisation de l'objet JSON a retourné
  let db;
  //Déclaration de la querry pour la recherche des ruches
  let query = {"selector":{"_id": {"$gt": "0"}},"fields": ["_id", "nom", "localisation"],"sort": [{"_id": "asc"}]};

  //Déclarartion de la Base de Donnée en ciblant la database ruches
  try{
    db = cloudant.db.use(DBName.ruches);
  }catch (err)
  {
    console.log('[api/mesures : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }

  let IdRuches = []; //Déclaration d'un array vide

  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    //console.log(result);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests api/mesures]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      //la Base retourne des élements
      IdRuches = result.docs;
    }
    else {
      //retour_json = {"error": true, "message": "Aucune Ruches enregistrée", "code": 503};
      //return retour_json;
    }
  }).catch((err) => {
    console.log('[api/mesures : \x1b[31mERROR\x1b[0m ruches] '+ getDateTime() + " -- " + err);
    return res.status(500).send({error : err});
  });

  //Chagement de base de données pour la récupération des mesures
  try{
    db = cloudant.db.use(DBName.mesures);
  }catch (err)
  {
    console.log('[api/mesures : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }
  let mesuresExt = {}; // Objet JSON temporaire pour stoker le resultat

  //Changement de query pour la recherche de la dèrnière mesure exterieur
  query = {"selector":{"source":{"$eq": "external" }},"fields": ["_id", "source", "date","temperature", "vent", "humidite", "lum"],"sort": [{"date": "desc"}],"limit":1};

  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests api/mesures]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      let mobjTemp = result.docs;
      mesuresExt.temperatureE = mobjTemp[0].temperature || ((mobjTemp[0].temperature === 0)?0:"N/A");
      mesuresExt.lum = mobjTemp[0].lum || ((mobjTemp[0].lum === 0)?0:"N/A");
      mesuresExt.humiditeeE = mobjTemp[0].humidite || ((mobjTemp[0].humidite === 0)?0:"N/A");
      mesuresExt.vent = mobjTemp[0].vent || ((mobjTemp[0].vent === 0)?0:"N/A");
      //dans le cas ou la valeur est undefined ou 0 l'éxecution se fait
      // à droite du ou "||", dans certaint cas si 0 ecrire 0 sinon false
    }
  }).catch((err) => {
    console.log('[api/mesures : \x1b[31mERROR DB\x1b[0m mesures] '+ getDateTime() + " -- " + err);
    return res.status(500).send({error : err});
  });

  for(let i = 0; i < IdRuches.length; i++) //Pour chaques ruches trouvé chercher les dèrnières mesures
  {
    //Déclaration d'un objet JSON Temporaire
    let element = {};

    //Changement de query pour la recherche de la dèrnière mesure intèrne a la ruche
    query = {"selector":{"source":{"$eq": IdRuches[i]._id }},"fields": ["_id", "source", "date", "temperature", "poids", "humidite", "abeille_in", "abeille_out"],"sort": [{"date": "desc"}],"limit":1};

    await db.find(query).then(result => {
      if (dev_logs === 1) {
        console.log("\x1b[36m%s\x1b[0m",'[Tests api/mesures]' + getDateTime() + ' -- retour Cloudant : \n');
        console.log(result.docs);
      }
      if (result.docs) {
        let mobjTemp = result.docs;
        element.id = mobjTemp[0].source;
        element.nom = IdRuches[i].nom || "N/A";
        element.localisation = IdRuches[i].localisation || false;
        element.date = mobjTemp[0].date || "N/A";
        element.temperatureI = mobjTemp[0].temperature || ((mobjTemp[0].temperature === 0)?0:"N/A");
        element.poids = mobjTemp[0].poids || ((mobjTemp[0].poids === 0)?0:"N/A");
        element.humiditeeI = mobjTemp[0].humidite || ((mobjTemp[0].humidite === 0)?0:"N/A");
        element.abeille_in = mobjTemp[0].abeille_in || ((mobjTemp[0].abeille_in === 0)?0:"N/A");
        element.abeille_out = mobjTemp[0].abeille_out || ((mobjTemp[0].abeille_out === 0)?0:"N/A");
      }
      else {
      }
    }).catch((err) => {
      console.log('[api/mesures : \x1b[31mERROR DB\x1b[0m mesures] '+ getDateTime() + " -- " + err);
      return res.status(500).send({error : err});
    });

    // Ajout des mesure extérieur au données de la ruche
    element.temperatureE = mesuresExt.temperatureE;
    element.lum = mesuresExt.lum;
    element.humiditeeE = mesuresExt.humiditeeE;
    element.vent = mesuresExt.vent;

    // Ajout de l'objet générer dans l'objet retournée
    retour_json.ruches.push(element);
    sleep(500);
  }
  if (dev_logs === 1) {
    console.log("\x1b[36m%s\x1b[0m",'[Tests api/mesures]' + getDateTime() + ' -- retour de la Route OK: \n');
  }
  console.log('END ' + getDateTime() + '] api/mesures \n');
  res.status(200).send(retour_json); // Renvois l'objet JSON complèt
});


/*********** Obtenir des valeurs sur une période *************/
app.get('/api/graph/:idGraph/:datedebut/:datefin', async (req, res) => {
  console.log('GET ' + getDateTime() + '] api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' \n');
  let retour_json = {data:[],external:[]} // Initialisation de l'objet JSON a retourné
  let idRuche = req.params.idGraph;
  let dateDebut = req.params.datedebut;
  let dateFin = req.params.datefin;
  let nomRuche;
  let db;
  /********************* Récupération du nom de la Ruche **********************/
  sleep(500);
  let query = {"selector":{"_id": {"$eq": idRuche}},"fields": ["nom"],"sort": [{"_id": "asc"}]};
  try{
    db = cloudant.db.use(DBName.ruches);
  }catch (err)
  {
    console.log('[api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }
  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests api/graph/:idGraph/:datedebut/:datefin]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      nomRuche = result.docs;
      nomRuche = nomRuche[0].nom;
    }
    else {
      nomRuche = 'N/A';
    }
  }).catch((err) => {
    console.log('\n\n[api/graph/:idGraph/:datedebut/:datefin : \x1b[31mERROR DB\x1b[0m ruches] '+ getDateTime() + " : \n " + err);
    nomRuche = 'N/A';
  });

  /********************* Récupération des donnes exterieur **********************/
  //Chagement de base de données pour la récupération des mesures
  try {
    db = cloudant.db.use(DBName.mesures);
  }catch (err)
  {
    console.log('[api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' : ERREUR DB] : ' + err)
    return res.status(500).send({error: err});
  }
  let mesures; // Objet JSON temporaire pour stoker le resultat

  //Changement de query pour la recherche de la dèrnière mesure exterieur
  query = {"selector":{"source":"external","date":{"$gt": dateDebut,"$lt": dateFin}},"fields": ["_id", "source", "date","temperature", "vent", "humidite", "lum"],"sort": [{"date": "asc"}]};

  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests api/graph/:idGraph/:datedebut/:datefin]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      mesures = result.docs;
    }
    else
    {
      mesures = 'empty'
    }
  }).catch((err) => {
    console.log('\n\n[api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' : \x1b[31mERROR DB\x1b[0m mesures] '+ getDateTime() + " : \n" + err);
    return res.status(500).send({error : err});
  });

  if (mesures != 'empty')
  {
    for(let i = 0; i < mesures.length; i++)
    {
      let verif = mesures[i];
      if (!verif.humidite) ((verif.humidite===0)?verif.humidite=0:verif.humidite="N/A");
      if (!verif.temperature) ((verif.temperature===0)?verif.temperature =0:verif.temperature="N/A");
      if (!verif.lum) ((verif.lum===0)?verif.lum=0:verif.lum="N/A");
      if (!verif.vent) ((verif.vent===0)?verif.vent=0:verif.vent="N/A");
      retour_json.external.push(verif);
    }
  } else {retour_json.external = [];} // Si aucune valeur sur la période

  sleep(1010);// PAUSE de 1 secondes

  /********************* Lecture des mesures de la ruche selectionner***********************/

  //Changement de query pour la recherche de la dèrnière mesure intèrne a la ruche
  query = {"selector":{"source":idRuche,"date":{"$gt": dateDebut,"$lt": dateFin}},"fields": ["source", "date", "temperature", "poids", "humidite", "abeille_in", "abeille_out"],"sort": [{"date": "asc"}]};

  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+']' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      mesures = result.docs;
    } else {
      mesures = 'empty';
    }
  }).catch((err) => {
    console.log('\n\n[api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' : \x1b[31mERROR DB\x1b[0m mesures] '+ getDateTime() + " : \n " + err);
    return res.status(500).send({error : err});
  });

  if (mesures != 'empty')
  {
    for(let i = 0; i < mesures.length; i++)
    {
      let verif = mesures[i];
      if (!verif.humidite) ((verif.humidite===0)?verif.humidite=0:verif.humidite="N/A");
      if (!verif.temperature) ((verif.temperature===0)?verif.temperature=0:verif.temperature="N/A");
      if (!verif.poids) ((verif.poids===0)?verif.poids=0:verif.poids="N/A");
      if (!verif.abeille_in) ((verif.abeille_in===0)?verif.abeille_in=0:verif.abeille_in="N/A");
      if (!verif.abeille_out) ((verif.abeille_out===0)?verif.abeille_out=0:verif.abeille_out="N/A");
      retour_json.data.push(verif);
    }
  } else {retour_json.data = [];} // Si aucune valeur sur la période

  retour_json.id = idRuche;
  retour_json.nom = nomRuche;
  retour_json.debut = dateDebut;
  retour_json.fin = dateFin;
  if (dev_logs === 1) {
    console.log("\x1b[36m%s\x1b[0m",'[Tests api/graph/:idGraph/:datedebut/:datefin]' + getDateTime() + ' -- retour Route OK : \n');
  }
  console.log('END ' + getDateTime() + '] api/graph/'+req.params.idGraph+'/'+req.params.datedebut+'/'+req.params.datefin+' \n');
  return res.status(200).send(retour_json); // Renvois l'objet JSON complèt
});

/*********** Lecture des alertes* ************/
app.get('/api/alert', async (req, res) => {
  console.log('GET ' + getDateTime() + '] /api/alert \n');
  let retour_json = {} // Initialisation de l'objet JSON a retourné
  let statut = 200;
  let messError = '';
  let db;
  //Déclaration de la querry pour la recherche des ruches
  let query = {"selector":{"_id": {"$gt": "0"}},"fields": ["aNBabeil","aTempI","aPoids","aHumidI","aHumidE","aVent","aLum"],"sort": [{"_id": "asc"}]};

  //Déclarartion de la Base de Donnée en ciblant la database ruches
  try{
    db = cloudant.db.use(DBName.alerts);
  }catch (err)
  {
    console.log('[GET api/alert : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }

  let alertes;
  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests GET api/alert]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      //la Base retourne des élements
      alertes = result.docs;
    }
    else {
      //retour_json = {"error": true, "message": "Aucune Ruches enregistrée", "code": 503};
      //return retour_json;
    }
  }).catch((err) => {
    console.log('[\x1b[31mERROR\x1b[0m GET api/alert] '+ getDateTime() + " -- " + err);
    messError = '[ERROR GET api/alert] '+ getDateTime() + " -- " + err;
    statut = 404;
  });
  if (alertes === undefined)
  {
    statut = 404;
  }
  else {
    retour_json.aNBabeil = alertes[0].aNBabeil || ((alertes[0].aNBabeil === 0)?0:"N/A");
    retour_json.aTempI = alertes[0].aTempI || ((alertes[0].aTempI === 0)?0:"N/A");
    retour_json.aPoids = alertes[0].aPoids || ((alertes[0].aPoids === 0)?0:"N/A");
    retour_json.aHumidI = alertes[0].aHumidI || ((alertes[0].aHumidI === 0)?0:"N/A");
    retour_json.aHumidE = alertes[0].aHumidE || ((alertes[0].aHumidE === 0)?0:"N/A");
    retour_json.aVent = alertes[0].aVent || ((alertes[0].aVent === 0)?0:"N/A");
    retour_json.aLum = alertes[0].aLum || ((alertes[0].aLum === 0)?0:"N/A");
  }
  if (statut != 200)
  {
    res.status(statut).send({error: ('ERROR ' + statut + " ==> " + messError)});
  }else {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests GET api/alert]' + getDateTime() + ' -- Retour de Route OK : \n');
    }
    console.log('GET ' + getDateTime() + '] /api/alert \n');
    res.status(statut).send(retour_json);
  }
});


/*********** Modification du statut des alertes *************/
app.post('/api/alert',authToken, async (req, res) => {
  console.log('POST ' + getDateTime() + '] /api/alert \n');
  let aNBabeil = req.body.aNBabeil;
  let aTempI = req.body.aTempI;
  let aPoids = req.body.aPoids;
  let aHumidI = req.body.aHumidI;
  let aHumidE = req.body.aHumidE;
  let aVent = req.body.aVent;
  let aLum = req.body.aLum;
  let db;
  let alert = {
    _id: "i/alertes",
    aNBabeil: ((aNBabeil)?aNBabeil:0),
    aTempI: ((aTempI)?aTempI:0),
    aPoids: ((aPoids)?aPoids:0),
    aHumidI: ((aHumidI)?aHumidI:0),
    aHumidE: ((aHumidE)?aHumidE:0),
    aVent: ((aVent)?aVent:0),
    aLum: ((aLum)?aLum:0)
  }
  let query = {"selector":{"_id": {"$eq": "i/alertes"}},"fields": ["_id","_rev"],"sort": [{"_id": "asc"}]};
  try{
    db = cloudant.db.use(DBName.alerts);
  }catch (err)
  {
    console.log('[POST api/alert : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests POST api/alert]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      let data = result.docs;
      if (data.length != 0)
      {
        alert._rev = data[0]._rev
      } else {
      }
    }
  }).catch((err) => {
    console.log('\n\n[POST api/alert : \x1b[31mERROR DB\x1b[0m ruches] '+ getDateTime() + " : \n " + err);
    return res.status(500).send({error : err});
  });
  console.log('END ' + getDateTime() + '] /api/alert \n');
  db.insert(alert, function(err, body) {
    if (err) return res.status(500).send({error : err});
    else return res.status(200).send({message: 'alerts edited'});
  });
})


/*********** Modification des informations sur la ruche *************/
app.put('/api/edit/:idRuche',authToken, async (req, res) => {
  console.log('PUT ' + getDateTime() + '] /api/edit/'+ req.params.idRuche +'\n');
  const rucheid = req.params.idRuche // récuperation de l'ID de la ruche via l'URL
  let rucheData = {};
  let db;
  let query = {"selector":{"_id": {"$eq": rucheid}},"fields": ["_id","_rev","localisation","nom"],"sort": [{"_id": "asc"}]};
  try{
    db = cloudant.db.use(DBName.ruches);
  }catch (err)
  {
    console.log('[PUT api/edit/:idRuche : \x1b[31mERROR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }
  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests PUT api/edit/:idRuche]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      let data = result.docs;
      if (data.length != 0)
      {
        rucheData = data[0]
      } else {
        return res.status(404).send({error : "La Ruche n'existe pas"});
      }
    }
  }).catch((err) => {
    console.log('\n\n[\x1b[31mERROR\x1b[0m api/edit/:idRuche : DB ruches] '+ getDateTime() + " : \n " + err);
    return res.status(500).send({error : err});
  });

  let old = {
    _id: rucheData._id,
    _rev: rucheData._rev,
    localisation: rucheData.localisation,
    nom: rucheData.nom
  };

  if (req.body.localisation) rucheData.localisation = req.body.localisation
  if (req.body.nom) rucheData.nom = req.body.nom

  console.log('END ' + getDateTime() + '] /api/edit/'+ req.params.idRuche +'\n');
  if (old === rucheData) return res.status(200).send({message: 'rien à changer'});
  db.insert(rucheData, function(err, body) {
    if (err) return res.status(500).send({error : err});
    else return res.status(200).send({new:rucheData,old:old});
  });
});


/***********Routes API Authentification*************/
// Permet de générer une Clé JWT pour l'utilisation des route soumise a authentification de l'utilisateur
app.post('/api/token', async (req, res) =>{
  // Ici on vérifierait que le login et mot de passe sont corrects
  console.log('POST ' + getDateTime() + '] api/token \n');
  try {
    let { username, password } = req.body;
    let mobjLogin = await login(username,password);
    if (mobjLogin.token) {
      res.send({token: mobjLogin.token })
    }
    else {
      if (mobjLogin.serverErr) return res.status(500).send({error: mobjLogin.error});
      return res.status(401).send({error: mobjLogin.error})
    }
  } catch (error) {
    res.status(400).send(error)
  }
  console.log('END ' + getDateTime() + '] api/token \n');
});


/********** Route supression des données d'une ruche *************/
app.delete('/api/rm/:idRuche',authToken, async (req, res) => {
  console.log('DELETE ' + getDateTime() + '] api/rm/'+req.params.idRuche+'\n');
  const rucheid = req.params.idRuche;
  let rucheData = {id:"",rev:""};
  let mesuresData;
  let db;
  let deletedDocuments = {data:[]};

  let query = {"selector":{"_id": {"$eq": rucheid}},"fields": ["_id","_rev"],"sort": [{"_id": "asc"}]};
  try {
    db = cloudant.db.use(DBName.ruches);
  }catch (err)
  {
    console.log('[DELETE api/rm : \x1b[31mERROR\x1b[0m DB] : ' + err)
    return res.status(500).send({error: err});
  }
  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests DELETE api/rm/:idRuche]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      let data = result.docs;
      if (data.length != 0)
      {
        rucheData.id = data[0]._id;
        rucheData.rev = data[0]._rev;
      } else {
        return res.status(404).send({error : "La Ruche n'existe pas"});
      }
    }
  }).catch((err) => {
    console.log('\n\n[DELETE api/rm : \x1b[31mERROR\x1b[0m DB ruches] '+ getDateTime() + " : \n " + err);
    return res.status(500).send({error : err});
  });
  try{
    db = cloudant.db.use(DBName.mesures);
  }catch (err)
  {
    console.log('[DELETE api/rm : \x1b[31mERROR\x1b[0m DB] : ' + err)
    return res.status(500).send({error: err});
  }

  //Changement de query pour la recherche de la dèrnière mesure exterieur
  query = {"selector":{"source":rucheid},"fields": ["_id", "_rev"]};
  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests DELETE api/rm/:idRuche]' + getDateTime() + ' -- retour Cloudant : \n');
      console.log(result.docs);
    }
    if (result.docs) {
      mesuresData = result.docs;
    }
    else
    {
      mesuresData = [];
    }
  }).catch((err) => {
    console.log('\n\n[DELETE api/rm : \x1b[31mERROR\x1b[0m DB mesures] '+ getDateTime() + " : \n" + err);
    return res.status(500).send({error : err});
  });

  for (let i = 0; i < mesuresData.length; i++)
  {
    try {
      db = cloudant.db.use(DBName.mesures);
    }catch (err)
    {
      console.log('[DELETE api/rm : \x1b[31mERROR DB\x1b[0m] : ' + err)
      return res.status(500).send({error: err});
    }
    db.destroy(mesuresData[i]._id, mesuresData[i]._rev, function(er, body){
      if (er) console.log('\x1b[31mERROR\x1b[0m m: %s', er)
      else {
        console.log('\x1b[31m[DELETE api/rm] element suprimer : \x1b[0m\n');
        console.log(body);

      }
    })
    sleep(100);
  }

  try {
    db = cloudant.db.use(DBName.ruches);
  }catch (err)
  {
    console.log('[DELETE api/rm : \x1b[31mERREUR DB\x1b[0m] : ' + err)
    return res.status(500).send({error: err});
  }
  db.destroy(rucheData.id, rucheData.rev, function(er, body){
    if (er) console.log('ERROR r: %s', er)
    else deletedDocuments.data.push(body)
  })
  console.log('END ' + getDateTime() + '] api/rm/'+req.params.idRuche+'\n');
  return res.status(200).send(deletedDocuments);
})


/***********Lancement du serveur express*************/
app.listen(port, () =>  { // ecoute du serveur sur le port 8080
  console.log('API REST 1.3.1 : Serveur Actif http://127.0.0.1'+((port == 80)?'':(':'+port))+"/")
  if (dev_logs === 1) console.log("\x1b[36m%s\x1b[0m",'\n /!\\ Les dev logs sont activer /!\\ \n')
});

/************************************ Fonctions de l'API **********************************/

// fonction pour obtenir le date pour le cration de logs
function getDateTime() {
  //initialisation de l'objet mobjDate de la classe Date
  let mobjDate = new Date();
  //Récupération des valeurs de temps
  let mtabDate= [
    mobjDate.getFullYear(),
    ((((mobjDate.getMonth()+1)) < 10)?"0"+((mobjDate.getMonth()+1)):((mobjDate.getMonth()+1))),
    (((mobjDate.getDate())< 10)?"0"+(mobjDate.getDate()):(mobjDate.getDate())),
    ((((mobjDate.getHours()-timezone_offset)) < 10)?"0"+((mobjDate.getHours()-timezone_offset)):((mobjDate.getHours()-timezone_offset))),
    (((mobjDate.getMinutes()) < 10)?"0"+(mobjDate.getMinutes()):(mobjDate.getMinutes())),
    (((mobjDate.getSeconds()) < 10)?"0"+(mobjDate.getSeconds()):(mobjDate.getSeconds()))
  ];

  //conversion en chaine str format JSON UTC
  let mstrDate = mtabDate[0] + "-" +
  mtabDate[1] + "-" +
  mtabDate[2] + "T" + mtabDate[3] + ":" +
  mtabDate[4] + ":" + mtabDate[5]+ ".000Z";

  return mstrDate;
}
// Fonction pour faire une pause
function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
//Vérification du tocken envoyer par le Client pour la route indiquée
function authToken(req, res, next) {
  const authHeader = req.headers.authorization;
  if (authHeader) {
    const token = authHeader.split(' ')[1];
    jwt.verify(token, process.env.JWT_KEY, (err, user) => {
      if (err) {
        return res.status(403).send({error : err});
      }
      req.user = user;
      if (dev_logs === 1) console.log("\x1b[36m%s\x1b[0m",'[AUTH]' + getDateTime() + ' OK \n');
      next();
    });
  } else {
    return res.sendStatus(401);
  }
}
async function login(user,pass){
  let db;
  let query = {"selector":{"_id": {"$gt": "0"}},"fields": ["username", "password","role"],"sort": [{"_id": "asc"}]};
  let retour = {
    error: "0"
  };
  let db_users;
  try {
    db = cloudant.db.use(DBName.users);
  }catch (err)
  {
    console.log('[login() : \x1b[31mERROR DB\x1b[0m] : ' + err)
    retour.error = err;
    retour.serverErr = 1;
    return retour;
  }

  //lecture dans la base de données via le query définit
  //le mot clé async empèche le programme de poursuivre le code pandant le traitement
  await db.find(query).then(result => {
    db_users = result.docs;
  }).catch((err) => {
    console.log('[\x1b[31mERROR\x1b[0m] '+ getDateTime() + " -- " + err);
    retour.error = err;
    retour.serverErr = 1;
    return retour; // other Error
  });

  // Recherche dans la liste de l'utilisateur
  for (let i = 0; i < db_users.length ; i++)
  {
    if (db_users[i].username === user)
    {
      if (db_users[i].password === pass)
      {
        retour.token = jwt.sign({username: db_users[i].username, role: db_users[i].role}, process.env.JWT_KEY, { expiresIn: '1800s'}); // { expiresIn: '1800s'}
        retour.error = "";
        if (dev_logs === 1) console.log("\x1b[36m%s\x1b[0m",'[login()]' + getDateTime() + ' OK \n');
        return retour;
      }
      else {
        retour.error = "Wrong Credential Informations"
        return retour
      }
    }
  }
  retour.error = "Wrong Credential Informations"
  return retour
}

//Verification que les database existe sur le Service Cloudant
async function initBases(){
  //Vérification des databases Cloudant
  let dbList;
  await cloudant.db.list().then((body) => {
    dbList = body;
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- retour liste des databases Cloudant : \n');
      console.log("\x1b[36m%s\x1b[0m",dbList + '\n\n');
    }
  })
  let available = false //valeur de Contrôle
  // Vérification Mesures
  for(let i = 0; i < dbList.length; i++)
  {
    if (dbList[i] === DBName.mesures) available = true;
  }
  if (available === false){
    await cloudant.db.create(DBName.mesures);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- DB mesures crée : \n');
    }
    let db = cloudant.use(DBName.mesures)
    let data = {
      "_id": "_design/b5ba6658697548d56bc3f3bff7d0889fe6c2ad7f",
      "language": "query",
      "views": {
        "foo-json-index": {
          "map": {
            "fields": {
              "date": "desc"
            },
            "partial_filter_selector": {}
          },
          "reduce": "_count",
          "options": {
            "def": {
              "fields": [
                {
                  "date": "desc"
                }
              ]
            }
          }
        }
      }
    }
    db.insert(data, function(err, body) {
      if (err) {
        console.log('ERREUR l\'index pour la db mesures n\'a pas pu être crée : ajouter manuelement l\'index dans la db via le dashboard Cloudant : \n \n');
        console.log(data);
        console.log('\n\n');
      }else {
        if (dev_logs === 1) {
          console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- Index de tri Implater dans la DB mesures: \n');
        }
      }
    });
  }
  available = false
  // Vérification Users
  for(let i = 0; i < dbList.length; i++)
  {
    if (dbList[i] === DBName.users) available = true;
  }
  if (available === false) {
    await cloudant.db.create(DBName.users);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- DB users crée : \n');
    }
  }
  available = false
  // Vérification Alertes
  for(let i = 0; i < dbList.length; i++)
  {
    if (dbList[i] === DBName.alerts) available = true;
  }
  if (available === false) {
    await cloudant.db.create(DBName.alerts);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- DB alerts crée : \n');
    }
  }
  available = false
  // Vérification Logs
  for(let i = 0; i < dbList.length; i++)
  {
    if (dbList[i] === DBName.logs) available = true;
  }
  if (available === false) {
    await cloudant.db.create(DBName.logs);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- DB logs crée : \n');
    }
  }
  available = false
  // Vérification Ruches
  for(let i = 0; i < dbList.length; i++)
  {
    if (dbList[i] === DBName.ruches) available = true;
  }
  if (available === false) {
    await cloudant.db.create(DBName.ruches);
    if (dev_logs === 1) {
      console.log("\x1b[36m%s\x1b[0m",'[Tests initBases()]' + getDateTime() + ' -- DB ruches crée : \n');
    }
  }
  available = false
  _useWorker('./autodel.js');
}

/*Ouverture d'un tread pour la boucle de supression Automatique*/
// ATTENTION CE CODE FONCTIONNE UNIQUEMENT SI l'OPTION --experimental-worker est indiquer au Lancement de l'application
const { Worker } = require('worker_threads') //modules pour la creation de Workers Js
/**
* Use a worker via Worker Threads module to make intensive CPU task
* @param filepath string relative path to the file containing intensive CPU task code
* @return {Promise(mixed)} a promise that contains result from intensive CPU task
*/
function _useWorker (filepath) { //Attend un string qui est le lien relative du fichier à lancer (fichier .js)
  return new Promise((resolve, reject) => {
    const worker = new Worker(filepath)// Creation du nouveau threads
    worker.on('online', () => { console.log('\x1b[33mLancement du Programme de Supression Automatique des données anciènne\x1b[0m') })
    worker.on('message', messageFromWorker => { //Déclancher lorsqu'on éxecute la commande postMessage sur le processus fils
      if (messageFromWorker.deleteElt){ //Cas instruction de supression envoyée dans postMessage
        deleteOld(messageFromWorker.deleteElt);
      } else {
      console.log('\x1b[33m--- Autodel.js ---\x1b[0m')
      console.log(messageFromWorker)
      }
      return resolve
    })
    worker.on('error', reject) // Si ERREUR processus
    worker.on('exit', code => { // Si fin du processus
      if (code !== 0) {
        reject(new Error(`\x1b[33mWorker stopped with exit code ${code}\x1b[0m`))
      }
    })
  })
}

//Fonction de supression des elements obsolète Cette fonction est lié au worker-thread
// elle doit donc être executer uniquement dans la fonction _useworker,
//donnees est un array envoyer par autodel.js qui contien _il _rev et date sous forme d'objet json
async function deleteOld(donnees) {
  let db = cloudant.db.use(DBName.mesures);
  for (let i = 0; i < donnees.length; i++)
  {
    db.destroy(donnees[i]._id, donnees[i]._rev, function(er, body){
      if (er)  console.log('\x1b[31mERROR\x1b[0m m: %s', er)
      else {
         console.log('\x1b[31m[DELETE] element : _id: '+donnees[i]._id+' _rev: '+donnees[i]._rev+' date: '+donnees[i].date+'\x1b[0m\n');
      }
    })
    sleep(100);
  }
}

/********************* API INFO ******************/
//permet d'afficher sur une page simple en HTML les information sur l'API Rest
app.get('/api/version', async (req, res) =>{
  console.log('GET ' + getDateTime() + '] api/version \n');
  let data;
  try {
    data = fs.readFileSync('./infoapi.html', 'utf8');
  } catch (err) {
    data = err;
  }
  console.log('END ' + getDateTime() + '] api/version \n');
  res.send(data);
})
