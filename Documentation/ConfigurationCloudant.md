[Configuration IBM](./ConfigurationIBM.md)

**Cloudant :**

Tout comme Watson rendez-vous sur la page principale du service Cloudant.

Nous allons nous rendre dans l’onglet Données d’identification pour le service et crée une nouvelle donnée d’identification, après sa création vous obtiendrez un texte format JSON comme suit les informations qui vont nous intéresser pour la configuration de l’API sont « password » et « username » (Utile aussi pour nodeRED).

![Cloudant Credentials](./assets/cloudant_cred.png)

Ensuite retournez sur l’onglet Gérer puis sur le bouton « Launch Dashboard »

Vous arrivez sur le dashboard de votre base de données.

![Cloudant Dashboard](./assets/Cloudant_dashboard.png)

Vous allez crée une base de données "users".

Choisissez « non-partitioned » dans le choix qui vous est proposée (vous pouvez aussi changer les noms proposer nous vous montrerons comment les configurer dans l’API (comme dans l’exemple sauf le nom c'est "users").

![Cration Base](./assets/cree_database.png)

Rentrez ensuite dans la database pour y ajouter un utilisateur manuellement.
Dans l'interface cherchez le bouton Create Document puis cliquez dessus

Indiquez les valeurs comme suit (les vôtres) vous pouvez indiquer le rôle si vous le souhaité, il s'agit d'une amélioration éventuel qui peut être fait dans l'API au niveau de la gestion des accès au routes avec JWT.

![Cration Document](./assets/cree_doc_user.png)

Et tout est près pour le déploiement de l'API !

[Déploiement sur IBM Cloud](../README.md#déploiement-sur-ibm-cloud)

---

_version du 06/05/2020 |_
_HUDON Guillaume_
