# Configuration d'IBM Cloud

Configurer IBM Cloud pour le Déploiement de l'Application


-	Crée un compte ou se connecter sur http://cloud.ibm.com
-	Une fois l’inscription ou la connexion terminée on arrive sur le Dashboard d’IBM Cloud :
![Dashboard](./assets/dashboard.png)
-	Cliquer sur Crée une ressource

Pour les besoin de l’application nous allons devoir installer deux services :
-	Cloudant
-	Watson

![Cloudant](./assets/cloudant_elt.png) ![Watson](./assets/wat_elt.png)

Pour chaque services il faut choisir la localisation des serveurs de préférence choisissez la même région pour l’ensembles des services.

Il faut ensuite choisir le plan pour ce service dans le cadre du projet BTS les plan Lite on été choisi (Attention avec les plan gratuit qui ne sont pas forcement gratuit ou sont moins avantageux que le plan lite).
Si vous le souhaite vous pouvez définir un nom ou laissé le nom par default

Une fois les informations entrée vous pouvez appuyer sur le bouton suivant :

![Cree un Service](./assets/cree_service.png)

**IBM Watson :**

Nous allons configurer IBM Watson retournez sur le Dashboard puis sélectionnez service et Internet of Things :

![Services](./assets/services.png)

![Liste des Services IBM](./assets/list_services.png)

Vous arriverez sur la page d’accueil du service appuyez sur Lancer :

![Lancer Watson](./assets/run_watson.png)

Dans la plateforme d’IBM Watson ce qui va nous intéresser ici c’est la configuration de terminaux ici on va voir comment en crée un, dans notre système les ruches sont représenter par une carte central qui envois directement au broker de Watson il lui faut donc un terminal Machine pour pouvoir communiquer avec l’API. L’API doit également avoir son terminal pour pouvoir récupérer ces informations transmise.

Donc sur Watson vous arrivez sur la page des terminaux il suffit de cliquer sur le bouton en haut à droite « Ajouter un terminal » :

![Watson Dashboard](./assets/watson_dashboard.png)

Indiquer donc le type de terminal et son nom :

![Crée un Terminal](./assets/cree_term.png)

Pour l’onglet suivant on vous demande de rentrer des informations complémentaire sur l’appareil vous pouvez le faire si vous le souhaité mais cela n’aura aucun impact sur les configurations à venir.

Faite donc suivant et on vous demandera de définir un token si vous laissez le champ vide un token sera automatiquement générer par le service :

![Crée un Terminal 2](./assets/cree_term2.png)

Faite suivant et vous arriverez sur une page récapitulatif fait « Terminer » et, vous sera afficher, les informations du terminal. Notez bien ces informations quelque part elles vous seront utile dans la configuration de la Carte embarqué sans ces informations vous ne pourrez pas vous connecter sur le service via la carte.

![Crée un Terminal 3](./assets/cree_term3.png)

If faut ensuite crée une clé API pour le logiciel NodeRED (elle n'est pas dans l'API il s'agit d'une application à crée à part voir installation de NodeRED sur IBM Cloud dans les Documentations du Projet).

Pour crée une Clé d'API rendez-vous dans l'onglet Application dans le menu a Gauche, puis crée une clé API pour une Application Standard notez le clé et le token pour configurer NodeRED


[Configuration Coudant](./ConfigurationCloudant.md)

Si vous avez déjà configurer Cloudant tout est près pour le déploiement de l'API !

[Déploiement sur IBM Cloud](../README.md#déploiement-sur-ibm-cloud)

---

_version du 06/05/2020 |_
_HUDON Guillaume_
