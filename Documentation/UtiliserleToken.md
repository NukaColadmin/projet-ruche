# Utiliser le Token pour les Routes Sécuriser (Développement)

Pour pouvoir utiliser le token reçu par la route /api/token
Il faut la placer dans les headers de la requête HTTP dans
la valeur ```Authorization``` avec comme argument :
~~~~
Bearer <token>
~~~~

---

version du 06/05/2020 | HUDON Guillaume
