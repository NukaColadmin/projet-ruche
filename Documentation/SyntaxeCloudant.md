# Syntaxe Cloudant (Du moins ce que j'ai Compris)

Voici quelques infos sr Cloudant que j'ai pus récupérer par-ce par-la dans des forum ou des fois sur le site d'IBM

Forme global d'une query
~~~~json
{
   "selector": {
      "username": {
         "$eq": "DidierDrogba"
      }
   },
   "fields": [
      "_id",
      "_rev"
   ],
   "sort": [
      {
         "_id": "asc"
      }
   ]
}
~~~~

```selector``` c'est pour selectionner les élement ou username est égal à "DidierDrogba".

```field``` afficher uniquement ```_id``` et ```_rev```

```sort``` Trier les réponse dans l'ordre croissant (création du document je pense, du plus ancien au plus récent)

---

Lire dans l'ordre décroissant (nécessite l'ajout d'un index):

~~~~json

   "sort": [
      {
         "_id": "desc"
      }
   ]
~~~~

exemple d'Index pour cette lecture :
~~~~json
{
  "_id": "_design/b5ba6658697548d56bc3f3bff7d0889fe6c2ad7f",
  "_rev": "2-b0373832524e891ca9dd895178b86a40",
  "language": "query",
  "views": {
    "foo-json-index": {
      "map": {
        "fields": {
          "date": "desc"
        },
        "partial_filter_selector": {}
      },
      "reduce": "_count",
      "options": {
        "def": {
          "fields": [
            {
              "date": "desc"
            }
          ]
        }
      }
    }
  }
}
~~~~

On peut pas _purge avec Cloudant

---

_version du 06/05/2020 | HUDON Guillaume_
