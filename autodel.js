//////////////////////////////////////////
// Logiciel d'auto supression des mesures obsolète
// HUDON Guillaume
// version 1.0.0.0 de l'api 1.3.0
//////////////////////////////////////////


require('dotenv').config(); // Environnement de l'API (définit dans .env ou sur l'interface cloud de l'application)
let Cloudant = require('@cloudant/cloudant'); // API Cloudant service IBM Cloud
const dev_logs = parseInt(process.env.DEV_LOG, 10) || 0; //Variable de debug pour voir des console log suplémentaire
const timezone_offset = process.env.timezone_offset || 0;
const { parentPort } = require('worker_threads')
const fs = require('fs'); //Manipulation de fichiers

let DBName;
try {
  let data = fs.readFileSync('./databaseList.json', 'utf8'); // Lecture du fichier
  DBName = JSON.parse(data);
} catch (err) {
  parentPort.postMessage(err);
}

var username = process.env.cloudant_username || "nodejs"; //définition credential user pour cloudant
var password = process.env.cloudant_password; // définition crédential password pour Cloudant
var cloudant = Cloudant({ account:username, password:password }); //Création de l'objet Cloudant


parentPort.postMessage('\x1b[32m[autodelscript Start]' + getDateTime() + '\x1b[0m\n');
let datelock = 'init'; //Bloquer le lecture pour en faire une par jour
main();
async function main() {
  while(true){
    sleep(1000)
    let datecheck = new Date()
    if (datelock === 'init' || datecheck.getDate != datelock.getDate)
    {
      let date = getDateTime(1);
      parentPort.postMessage('\x1b[31mLes éléments inferieur à cette date vont être suprimer : ' + date + '\x1b[0m\n');

      let donnees;
      let db = cloudant.db.use(DBName.mesures);
      let query = {"selector":{"date": {"$lt": date}},"fields": ["_id", "_rev","date"],"sort": [{"date": "desc"}]};
      await db.find(query).then(result => {
        donnees = result.docs;
        if (dev_logs === 1) parentPort.postMessage(donnees)
      }).catch((err) => {
        parentPort.postMessage('[\x1b[31mERROR\x1b[0m] '+ getDateTime() + " -- " + err);
      });
      if(donnees.length < 1) parentPort.postMessage('rien à suprimer');
      else{
        parentPort.postMessage({deleteElt: donnees})
      }
      datelock = new Date();
    }
  }
}
function getDateTime(retardY = 0, retardM = 0, retardD = 0, retardH = 0, retardmin = 0, retardS = 0) {
  //initialisation de l'objet mobjDate de la classe Date
  let mobjDate = new Date();
  //Récupération des valeurs de temps
  let mtabDate= [
    mobjDate.getFullYear() - retardY,
    (((mobjDate.getMonth()  - retardM) < 10)?"0"+(mobjDate.getMonth() - retardM):(mobjDate.getMonth() - retardM)),
    (((mobjDate.getDate() - retardD)< 10)?"0"+(mobjDate.getDate() - retardD):(mobjDate.getDate() - retardD)),
    ((((mobjDate.getHours()-timezone_offset) - retardH) < 10)?"0"+((mobjDate.getHours()-timezone_offset) - retardH):((mobjDate.getHours()-timezone_offset) - retardH)),
    (((mobjDate.getMinutes() - retardmin) < 10)?"0"+(mobjDate.getMinutes() - retardmin):(mobjDate.getMinutes() - retardmin)),
    (((mobjDate.getSeconds() - retardS) < 10)?"0"+(mobjDate.getSeconds() - retardS):(mobjDate.getSeconds() - retardS))
  ];
  //conversion en chaine str format JSON UTC
  let mstrDate = mtabDate[0] + "-" +
  mtabDate[1] + "-" +
  mtabDate[2] + "T" + mtabDate[3] + ":" +
  mtabDate[4] + ":" + mtabDate[5]+ ".000Z";

  return mstrDate;
}

function sleep(milliseconds) {
  const date = Date.now();
  let currentDate = null;
  do {
    currentDate = Date.now();
  } while (currentDate - date < milliseconds);
}
